﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FailCollider : MonoBehaviour
{
    public Text textbox;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            textbox.color = Color.white;
            StartCoroutine(ReloadGame());
        }
    }

    IEnumerator ReloadGame()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("MainLevel");
    }
}
