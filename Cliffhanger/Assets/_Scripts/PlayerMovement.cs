﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //Direction of the stick
    float upDown = 0.0f;
    float leftRight = 0.0f;
    string hand = "2";

    //Start is called before the first frame update
    private void Start()
    {
        //Choose the right hand
        if (gameObject.name == "upperarm_l")
        {
            hand = "1";
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //Get the current position of the stick
        upDown = Input.GetAxisRaw("Vertical " + hand);
        leftRight = Input.GetAxisRaw("Horizontal " + hand);
        Move(upDown, leftRight);
    }
    void Move(float vert, float hori)
    {
        float angle;
        if (hori != 0.0f ||  vert!= 0.0f)
        {
            angle = Mathf.Atan2(vert, hori) * Mathf.Rad2Deg;
            //print(angle);
            //THe direction of the stick is where the player's arm points
            this.transform.localEulerAngles = new Vector3(0, angle, 0);
        }
    }
}
