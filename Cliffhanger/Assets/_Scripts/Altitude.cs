﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Altitude : MonoBehaviour
{
    public float altitude;
    private float n_altitude;
    public Slider height;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Get the height of the player and show that on the slider
        altitude = GameObject.Find("Marionette").transform.position.y;
        n_altitude = altitude - 4;
        height.value = n_altitude;
    }
}
