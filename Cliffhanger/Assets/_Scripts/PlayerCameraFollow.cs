﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraFollow : MonoBehaviour
{
    //Set up variables
    public float smoothing = 1f;
    public GameObject player;
    Vector3 offset;

    void Start()
    {
        //set offset to the initial distace between the camera and the player
        offset = transform.position - player.transform.position;
    }

    private void FixedUpdate()
    {

        //define the target position for the camera to be the player's current position modified by the offset
        Vector3 targetCamPos = player.transform.position + offset;
       //zero out the x and z components of the target position, so that the camera only travels up and down
        targetCamPos.x = transform.position.x;
        targetCamPos.z = transform.position.z;
        //interpolate the camera's position at the speed of smoothing between its current and target position
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }

}
