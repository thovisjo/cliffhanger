﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandGrab : MonoBehaviour
{
    string hand = "2";
    string which_hand = "";
    Vector3 pos;

    private void Start()
    {
        //Choose the correct hand to use
        if(gameObject.CompareTag("hand_l"))
        {
            hand = "1";
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("HandHold"))
        {
            if (Input.GetButton("Fire" + hand))
            {
                grab(other);
            }
        }
    }
    private void grab(Collider handhold)
    {
        // moves hand to handhold and pins it there
        if(hand == "1")
        {
            which_hand = "controller_hand_left";
        }
        else
        {
            which_hand = "controller_hand_right";
        }
        gameObject.transform.position = handhold.transform.position;

    }
}
