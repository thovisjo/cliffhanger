# Cliffhanger

Hello New Group!
	Here is some super sweet information that you should know with “Cliffhanger” moving forward into the project.
Unity Version: 2019.2.4f1
Controller Scheme: 
	The character within the game is controlled through the use of a ps3 or xbox 360 controller, we recommend using a ps3 controller because that is what we used in the early stages of development in the game. Use the thumbsticks to control the arms of the character to reach the climbing points. Right thumbstick for the right arm and left for the other. Use the bumpers on the controllers to grip the points on the wall. 

Moving Forward:
Our goal was to make a game where the player would climb an obscenely tall wall with some pretty whacky movement controls. One mechanic that we didn’t get into the base prototype was a “grip meter” which would limit the amount of time the player could hold onto a specific point on the wall. After the meter expires, they hand would let go and the player would begin to fall, but the player would be able to catch themselves before they hit the ground. Thus “Shaking Off” their minor setback.

Bugs:
There is a bug right now within the code where the left arm is not entirely functioning. We tried to resolve this, but could not figure it out. 
The Ragdoll also decides to throw itself out of control and clips through the wall, issue might be within the ragdoll collisions. 
